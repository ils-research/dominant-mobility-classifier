# Dominant Mobility Classifier #
This plugin classifies neighbourhoods based on their dominant mobility in walking, transit and car-dependent neighbourhoods. 

## Installation 
The plugin is available via the official QGIS Python Plugins Repository [Links will be published later] and is currently assigned as experimental. Further, it is possible to load the current version via Gitlab [Links will be published later]. Download the ZIP archive from the master branch and copy it to your local QGIS plugin directory:

* Linux: /.local/share/QGIS/QGIS3/profiles/default/python/plugins 
* Windows: C:\Users\USER\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins

After restarting QGIS the OS Walk-EU could be activated over the Plugin Manager. 

## Requirements
To use this plugin it is necessary to install the OpenRouteService (ORS) QGIS Plugin and sign-up to create and account and get your personal API-key in the dashboard. The current version of this tool allows calculating 500 units per 20 minutes for free. To process larger areas, we recommend to install a own version of ORS on a server. In addition, a valid GTFS dataset is required for the study area. See subchapter Access to public transport with high frequencies for more information. 
**Important: Set your preferred API key in the first place in the ORS plugin settings.**


## Usage

### Functionality
The evaluation of the mobility option on neighborhood level is identified by five indicators: 
•	Proximity of facilities and services
•	Functional land use mix
•	Share of green spaces
•	Design of the pedestrian network
•	Access to public transport with high frequencies

**For detailed methodological specifications, thresholds and interpretations of the results, please take a look at this article [Link to potential publication]**. In the subsections, only the data-related context is explained. 

#### Proximity of facilities and services
To rate proximity to key infrastructure, the ORS creates a 15-minute isochrones (by foot). The infrastructures are exported from OSM (OpenStreeetMaps) using the Overpass API. Here, the individual POI's are combined into categories (see table [link to attached table]).

#### Functional land use mix
To rate the land use mix, we make use of the SHDI (Shannon’s Diversity Index). The land use categories are exported form OSM (OpenStreetMaps) using the Overpass API. Similar to POI, land uses are also divided into classes (see table [link to attached table]).

#### Design of pedestrian network
The design of the pedestrian network is indicated by the pedestrian-shed. This indicator sets the 15-minute isochrones from ORS in relation to a static buffer.	

#### Access to public transport with high frequencies
This indicator uses GTFS (General Transit Feed Specification) datasets to generate georeferenced stations with frequency information. These datasets are often provided as Open Data by local transit agencies. Whether and for what period the feeds are available can be checked via various platforms (e.g. https://www.transit.land/ or https://transitfeeds.com/). 

### Parameters
**Input Geometry**: This tool calculates the mobility options on neighborhoods level. You can use your own geometries (e.g. city blocks), create your own grid via QGIS or download such a grid from official sources (e.g. https://inspire-geoportal.ec.europa.eu for European countries).

**Input GTFS-Data**: Set the folder location of the unzipped GTFS datasets.

**Output Folder location**: Set an output folder location where the results will be saved. 
